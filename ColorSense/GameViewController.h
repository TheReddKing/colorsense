//
//  GameViewController.h
//  ColorSense
//
//  Created by Kevin Fang on 7/12/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController {
    float maxHeight;
    float maxWidth;
    int nextButton;
    NSMutableArray* buttons;
    NSMutableArray* connections;
    NSMutableArray* availableColors;
    NSMutableArray* pressedButtons;
    
    NSMutableArray* requirements;
}

@end
