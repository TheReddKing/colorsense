//
//  GameViewController.m
//  ColorSense
//
//  Created by Kevin Fang on 7/12/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "GameViewController.h"
#import "RDGameButton.h"

@interface GameViewController () {
    
}

@end

@implementation GameViewController
NSArray* fullColors;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    maxWidth = self.view.bounds.size.width;
    maxHeight = self.view.bounds.size.height;
    
    fullColors = @[[UIColor colorWithRed:0.3 green:0.2 blue:0.4 alpha:1.0],[UIColor colorWithRed:0.6 green:0.2 blue:0.4 alpha:1.0],[UIColor colorWithRed:0.255 green:0.600 blue:0.158 alpha:1.0],[UIColor colorWithRed:0.928 green:0.894 blue:0.151 alpha:1.000],[UIColor colorWithRed:0.210 green:0.423 blue:1.000 alpha:1.000],[UIColor colorWithRed:1.000 green:0.470 blue:0.000 alpha:1.000]];
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated {
    
    [self loadGame];
}

- (void) loadGame {
    
    nextButton = 0;
    
    buttons = [[NSMutableArray alloc] init];
    availableColors = [[NSMutableArray alloc] initWithArray:fullColors];
    pressedButtons = [[NSMutableArray alloc] init];
    connections = [[NSMutableArray alloc] init];
    [self createButton];
    [self createButton];
    [self createConnection];
    [self createButton];
    [self createButton];
    [self createConnection];
}

- (void) createButton {
    int random = arc4random() % [availableColors count];
    UIColor* color = [availableColors objectAtIndex:random];
    [availableColors removeObjectAtIndex:random];
    RDGameButton* button = [[RDGameButton alloc] initWithID:nextButton Frame:[self makeCGRect:nextButton] andColor: color];
    [button addTarget:self action:@selector(buttonLifted:) forControlEvents:UIControlEventTouchUpInside];
    [button addTarget:self action:@selector(buttonLifted:) forControlEvents:UIControlEventTouchUpOutside];
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
    [buttons addObject:button];
    [self.view addSubview:button];
    nextButton++;
}

- (void) createConnection {
    if([buttons count] < 2) {
        NSLog(@"ERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR\nERROR");
    }
    
    int random = arc4random() % [availableColors count];
    UIColor* color = [availableColors objectAtIndex:random];
    [availableColors removeObjectAtIndex:random];
    NSArray* buttonGroup = [self choose:2];
    while (![RDGameConnection initWithButtons:buttonGroup toMake:color and:connections]) {
        buttonGroup = [self choose:2];
    }
    
}

- (NSArray*) choose : (int) howMany {
    
    NSMutableArray* pickedNames = [[NSMutableArray alloc] init];
    
    int remaining = howMany;
    
    if ( [buttons count] >= remaining )
    {
        while (remaining > 0)
        {
            id name = [buttons objectAtIndex: arc4random() % [buttons count]];
            
            if ( ! [pickedNames containsObject: name] )
            {
                [pickedNames addObject: name];
                remaining --;
            }
        }
    }
    return pickedNames;
}

- (void) buttonPressed : (RDGameButton*) button {
    [pressedButtons addObject:button];
    if([pressedButtons count] == 1) {
        [self.view setBackgroundColor:button.color];
    } else if([pressedButtons count] > 1) {
        for (RDGameConnection* connection in connections) {
            if([connection matches:pressedButtons]) {
                [self.view setBackgroundColor:connection.color];
                return;
            }
        }
        [self.view setBackgroundColor:[UIColor blackColor]];
    } else {
        NSLog(@"HMMMMMMMMMMMM");
    }
}
- (void) buttonLifted : (RDGameButton*) button {
    [pressedButtons removeObject:button];
    if([pressedButtons count] == 0) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGRect) makeCGRect: (int) v {
    //When v == 1 it is bottom left v == 2 bottom right v==3 above 1*
    
    float size = 80;
    int pos = v/2;
    if(v % 2 == 0) { //Left
        return CGRectMake(0 + size/10, maxHeight - (pos+1) * size - size/10, size, size);
    } else {
        return CGRectMake(maxWidth - size - size/10, maxHeight - (pos+1) * size - size/10, size, size);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
