//
//  RDGameButton.h
//  ColorSense
//
//  Created by Kevin Fang on 7/12/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDGameButton : UIButton {
    NSMutableArray* connections;
}
@property UIColor* color;

- (id) initWithID: (int) user Frame:(CGRect)frame andColor:(UIColor*) color;

@end

@interface RDGameConnection : NSObject

@property NSArray* objects;
@property UIColor* color;

+ (bool) initWithButtons: (NSArray*) buttons toMake: (UIColor*) color and: (NSMutableArray*) connections;
- (bool) matches: (NSArray*) objects;

@end