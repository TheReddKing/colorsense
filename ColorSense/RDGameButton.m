//
//  RDGameButton.m
//  ColorSense
//
//  Created by Kevin Fang on 7/12/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "RDGameButton.h"


@implementation RDGameButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
 */
- (id) initWithID: (int) user Frame:(CGRect)frame andColor:(UIColor*) colors {
    self = [super initWithFrame:frame];
    [self setBackgroundColor:colors];
    self.layer.cornerRadius = 50;
    self.color = colors;
    self.tag = user;
    self->connections = [[NSMutableArray alloc] init];
    return self;
}

@end

@implementation RDGameConnection

- (bool)matches:(NSArray *)objects {
    for ( RDGameButton* bbbb in objects) {
        if(![self.objects containsObject:bbbb]) {
            return false;
        }
    }
    return [objects count] == [self.objects count];
}

+ (bool) initWithButtons:(NSArray *)buttons toMake:(UIColor *)color and:(NSMutableArray *)connections  {
    
    for(RDGameConnection* connect in connections) {
        if([connect.objects isEqualToArray:buttons]) {
            return false;
        }
    }
    
    RDGameConnection* c = [[RDGameConnection alloc] init];
    c.objects = buttons;
    c.color = color;
    [connections addObject:c];
    return true;
}
@end